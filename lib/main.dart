import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sc_app/ui/episodes_screen/episodes_screen.dart';
import 'package:sc_app/ui/location_details_screen/location_details_screen.dart';
import 'package:sc_app/ui/locations_screen/locations_screen.dart';
import 'package:sc_app/ui/login_screen/login_screen.dart';
import 'package:sc_app/ui/settings_screen/settings_screen.dart';
import 'package:sc_app/ui/splash_screen.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/ui/characters_screen/characters_screen.dart';
import 'package:sc_app/utils/app_theme.dart';
import 'package:sc_app/widgets/init_widget.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return InitWidget(
      child: MaterialApp(
        routes: {
          CharactersScreen.routeName: (context) => const CharactersScreen(),
          SettingsScreen.routeName: (context) => const SettingsScreen(),
          LoginScreen.routeName: (context) => const LoginScreen(),
          LocationsScreen.routeName: (context) => const LocationsScreen(),
          LocationDetailsScreen.routeName: (context) =>
              const LocationDetailsScreen(),
          EpisodesScreen.routeName: (context) => const EpisodesScreen(),
        },
        theme: appTheme,
        debugShowCheckedModeBanner: false,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
        home: const SplashScreen(),
      ),
    );
  }
}
