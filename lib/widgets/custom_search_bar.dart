import 'package:flutter/material.dart';
import 'package:sc_app/utils/constants.dart';

class CustomSearchBar extends StatelessWidget {
  const CustomSearchBar({
    required this.onChanged,
    required this.hintText,
    super.key,
  });

  final ValueChanged<String>? onChanged;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            onChanged: onChanged,
            autofocus: false,
            style: AppStyles.s16w400,
            cursorColor: AppColors.mainText,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                vertical: 12,
                horizontal: 16,
              ),
              hintText: hintText,
              hintStyle: AppStyles.s16w400.copyWith(
                color: AppColors.neutral2,
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(36.0),
                borderSide: BorderSide.none,
              ),
              isDense: true,
              filled: true,
              fillColor: AppColors.neutral1,
              prefixIcon: const Icon(
                Icons.search,
                size: 24,
                color: AppColors.neutral3,
              ),
              suffixIcon: const Icon(
                Icons.filter_list_alt,
                size: 24,
                color: AppColors.neutral3,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
