import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/characters_bloc/characters_bloc.dart';
import 'package:sc_app/bloc/episodes_bloc/episodes_bloc.dart';
import 'package:sc_app/bloc/location_details_bloc/location_details_bloc.dart';
import 'package:sc_app/bloc/locations_bloc/locations_bloc.dart';
import 'package:sc_app/repository/api.dart';
import 'package:sc_app/repository/characters_repo.dart';
import 'package:sc_app/repository/episodes_repo.dart';
import 'package:sc_app/repository/locations_repo.dart';
import 'package:sc_app/repository/settings_repo.dart';

class InitWidget extends StatelessWidget {
  const InitWidget({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => Api(),
        ),
        RepositoryProvider(
          create: (context) => SettingsRepo(),
        ),
        RepositoryProvider(
          create: (context) => CharactersRepo(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        RepositoryProvider(
          create: (context) => LocationsRepo(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
        RepositoryProvider(
          create: (context) => EpisodesRepo(
            api: RepositoryProvider.of<Api>(context),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<CharactersBloc>(
            create: (context) => CharactersBloc(
              repo: RepositoryProvider.of<CharactersRepo>(context),
            )..add(const CharactersEvent.filterByName(name: '')),
          ),
          BlocProvider<LocationsBloc>(
            create: (context) => LocationsBloc(
              repo: RepositoryProvider.of<LocationsRepo>(context),
            )..add(
                const LocationsEvent.fetchLocations(),
              ),
          ),
          BlocProvider<LocationDetailsBloc>(
            create: (context) => LocationDetailsBloc(
              repo: RepositoryProvider.of<LocationsRepo>(context),
            ),
          ),
          BlocProvider<EpisodesBloc>(
            create: (context) => EpisodesBloc(
              repo: RepositoryProvider.of<EpisodesRepo>(context),
            )..add(
                const EpisodesEvent.fetchEpisodes(),
              ),
          ),
        ],
        child: child,
      ),
    );
  }
}
