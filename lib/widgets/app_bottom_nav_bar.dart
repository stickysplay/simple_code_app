import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/ui/characters_screen/characters_screen.dart';
import 'package:sc_app/ui/episodes_screen/episodes_screen.dart';
import 'package:sc_app/ui/locations_screen/locations_screen.dart';
import 'package:sc_app/ui/settings_screen/settings_screen.dart';
import 'package:sc_app/utils/app_assets.dart';
import 'package:sc_app/utils/app_colors.dart';
import 'package:sc_app/utils/helpers/no_animation_page_builder.dart';

class AppBottomNavigationBar extends StatelessWidget {
  const AppBottomNavigationBar({
    Key? key,
    required this.currentIndex,
  }) : super(key: key);

  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      items: [
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.characters,
            color: currentIndex == 0 ? AppColors.primary : AppColors.neutral2,
          ),
          label: S.of(context).characters,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.locations,
            color: currentIndex == 1 ? AppColors.primary : AppColors.neutral2,
          ),
          label: S.of(context).locations,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.episode,
            color: currentIndex == 2 ? AppColors.primary : AppColors.neutral2,
          ),
          label: S.of(context).episodes,
        ),
        BottomNavigationBarItem(
          icon: SvgPicture.asset(
            AppAssets.svg.settings,
            color: currentIndex == 3 ? AppColors.primary : AppColors.neutral2,
          ),
          label: S.of(context).settings,
        ),
      ],
      onTap: (index) {
        if (index == 0) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const CharactersScreen()),
            (_) => false,
          );
        } else if (index == 1) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const LocationsScreen()),
            (_) => false,
          );
        } else if (index == 2) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const EpisodesScreen()),
            (_) => false,
          );
        } else if (index == 3) {
          Navigator.of(context).pushAndRemoveUntil(
            NoAnimationPageRouteBuilder(route: const SettingsScreen()),
            (_) => false,
          );
        }
      },
    );
  }
}
