import 'package:flutter/material.dart';
import 'package:sc_app/utils/app_assets.dart';
import 'package:sc_app/utils/constants.dart';

class StateFailureWidget extends StatelessWidget {
  const StateFailureWidget({
    Key? key,
    required this.text,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(AppAssets.images.notFoundCharacter),
          const SizedBox(height: 28),
          Text(
            text,
            style: AppStyles.s16w400.copyWith(
              color: AppColors.neutral3,
            ),
          ),
        ],
      ),
    );
  }
}
