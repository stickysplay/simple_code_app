// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(characters) => "Total characters: ${characters}";

  static String m1(locations) => "Total locations: ${locations}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alive": MessageLookupByLibrary.simpleMessage("Alive"),
        "auth": MessageLookupByLibrary.simpleMessage("Authorization"),
        "characters": MessageLookupByLibrary.simpleMessage("Characters"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "create": MessageLookupByLibrary.simpleMessage("Create"),
        "dead": MessageLookupByLibrary.simpleMessage("Dead"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Do not have an account yet?"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterLoginAndPassword":
            MessageLookupByLibrary.simpleMessage("Enter login and password"),
        "episodes": MessageLookupByLibrary.simpleMessage("Episodes"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "female": MessageLookupByLibrary.simpleMessage("Female"),
        "human": MessageLookupByLibrary.simpleMessage("Human"),
        "incorrectLoginOrPassword":
            MessageLookupByLibrary.simpleMessage("Incorrect login or password"),
        "inputErrorCheckLogin":
            MessageLookupByLibrary.simpleMessage("Check login"),
        "inputErrorCheckPassword":
            MessageLookupByLibrary.simpleMessage("Check password"),
        "inputErrorLoginIsShort": MessageLookupByLibrary.simpleMessage(
            "Login must contain more than 3 symbols"),
        "inputErrorPasswordIsShort": MessageLookupByLibrary.simpleMessage(
            "Password must contain more than 8 symbols"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "locations": MessageLookupByLibrary.simpleMessage("Locations"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "male": MessageLookupByLibrary.simpleMessage("Male"),
        "noData": MessageLookupByLibrary.simpleMessage("No data"),
        "notFoundCharacter": MessageLookupByLibrary.simpleMessage(
            "A character with this name was not found"),
        "notFoundEpisode": MessageLookupByLibrary.simpleMessage(
            "Episode with this name was not found"),
        "notFoundLocation": MessageLookupByLibrary.simpleMessage(
            "Location with this name was not found"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "premier": MessageLookupByLibrary.simpleMessage("Premier"),
        "refresh": MessageLookupByLibrary.simpleMessage("Refresh"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "searchCharacter":
            MessageLookupByLibrary.simpleMessage("Search character"),
        "searchLocation":
            MessageLookupByLibrary.simpleMessage("Search location"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "signIn": MessageLookupByLibrary.simpleMessage("Sign in"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Somehing went wrong!"),
        "totalCharacters": m0,
        "totalLocations": m1,
        "unknown": MessageLookupByLibrary.simpleMessage("Unknown")
      };
}
