// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru_RU locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru_RU';

  static String m0(characters) => "Всего персонажей: ${characters}";

  static String m1(locations) => "Всего локации: ${locations}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "alive": MessageLookupByLibrary.simpleMessage("Жив(а)"),
        "auth": MessageLookupByLibrary.simpleMessage("Авторизация"),
        "characters": MessageLookupByLibrary.simpleMessage("Персонажи"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "create": MessageLookupByLibrary.simpleMessage("Создать"),
        "dead": MessageLookupByLibrary.simpleMessage("Мертв(а)"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("У вас еще нет аккаунта?"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterLoginAndPassword":
            MessageLookupByLibrary.simpleMessage("Введите логин и пароль"),
        "episodes": MessageLookupByLibrary.simpleMessage("Эпизоды"),
        "error": MessageLookupByLibrary.simpleMessage("Ошибка"),
        "female": MessageLookupByLibrary.simpleMessage("Женский"),
        "human": MessageLookupByLibrary.simpleMessage("Человек"),
        "incorrectLoginOrPassword": MessageLookupByLibrary.simpleMessage(
            "Введен неверный логин или пароль"),
        "inputErrorCheckLogin":
            MessageLookupByLibrary.simpleMessage("Проверьте логин"),
        "inputErrorCheckPassword":
            MessageLookupByLibrary.simpleMessage("Проверьте пароль"),
        "inputErrorLoginIsShort": MessageLookupByLibrary.simpleMessage(
            "Логин должен содержать не менее 3 символов"),
        "inputErrorPasswordIsShort": MessageLookupByLibrary.simpleMessage(
            "Пароль должен содержать не менее 8 символов"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "locations": MessageLookupByLibrary.simpleMessage("Локации"),
        "login": MessageLookupByLibrary.simpleMessage("Логин"),
        "male": MessageLookupByLibrary.simpleMessage("Межской"),
        "noData": MessageLookupByLibrary.simpleMessage("Нет данных"),
        "notFoundCharacter": MessageLookupByLibrary.simpleMessage(
            "Персонаж с таким именем не найден"),
        "notFoundEpisode": MessageLookupByLibrary.simpleMessage(
            "Эпизод с таким именем не найден"),
        "notFoundLocation": MessageLookupByLibrary.simpleMessage(
            "Локация с таким именем не найдена"),
        "password": MessageLookupByLibrary.simpleMessage("Пароль"),
        "premier": MessageLookupByLibrary.simpleMessage("Премьера"),
        "refresh": MessageLookupByLibrary.simpleMessage("Обновить"),
        "russian": MessageLookupByLibrary.simpleMessage("Русский"),
        "searchCharacter":
            MessageLookupByLibrary.simpleMessage("Найти персонажа"),
        "settings": MessageLookupByLibrary.simpleMessage("Настройки"),
        "signIn": MessageLookupByLibrary.simpleMessage("Войти"),
        "somethingWentWrong":
            MessageLookupByLibrary.simpleMessage("Что-то пошло не так!"),
        "totalCharacters": m0,
        "totalLocations": m1,
        "unknown": MessageLookupByLibrary.simpleMessage("Неизвестно")
      };
}
