import 'dart:developer';

import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/episode.dart';
import 'package:sc_app/repository/api.dart';

class EpisodesRepo {
  EpisodesRepo({required this.api});
  final Api api;

  Future<ResultEpisodeRepo> fetchEpisodes() => nextPage(1);

  Future<ResultEpisodeRepo> nextPage(int page) async {
    try {
      final result = await api.dio.get('/episode?page=$page');

      final bool isLastPage = result.data['info']['next'] == null;

      final List json = result.data['results'] ?? [];
      final episodesList = json.map((item) => Episode.fromJson(item));
      return ResultEpisodeRepo(
        episodesList: episodesList.toList(),
        isLastpage: isLastPage,
      );
    } catch (error) {
      log('FETCH_EPISODES_ERROR: $error');
      return ResultEpisodeRepo(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultEpisodeRepo {
  ResultEpisodeRepo({
    this.errorMessage,
    this.episodesList,
    this.isLastpage = false,
  });

  final String? errorMessage;
  final List<Episode>? episodesList;
  final bool isLastpage;
}
