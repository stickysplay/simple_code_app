import 'dart:developer';

import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/location.dart';
import 'package:sc_app/repository/api.dart';

class LocationsRepo {
  LocationsRepo({required this.api});

  final Api api;

  Future<ResultLocationRepo> fetchLocation(String? locationName) =>
      nextPage(1, locationName);

  Future<ResultLocationRepo> nextPage(int page, String? locationName) async {
    try {
      final result = await api.dio.get(
        '/location',
        queryParameters: {
          "name": locationName,
          "page": page,
        },
      );

      final bool isLastPage = result.data['info']['next'] == null;

      final List locationsJson = result.data['results'] ?? [];
      final locationsList = locationsJson.map(
        (location) => Location.fromJson(location),
      );
      return ResultLocationRepo(
        locationsList: locationsList.toList(),
        isLastPage: isLastPage,
      );
    } catch (error) {
      log('LOCATION_FETCH_ERROR: $error');
      return ResultLocationRepo(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }

  Future<ResultLocationRepo> fetchLocationDetails(int id) async {
    try {
      final result = await api.dio.get('/location/$id');
      final locationJson = result.data;
      final Location location = Location.fromJson(locationJson);
      return ResultLocationRepo(location: location);
    } catch (error) {
      log('LOCATION_FETCH_ERROR: $error');
      return ResultLocationRepo(errorMessage: S.current.somethingWentWrong);
    }
  }
}

class ResultLocationRepo {
  ResultLocationRepo({
    this.errorMessage,
    this.locationsList,
    this.location,
    this.isLastPage = false,
  });

  final String? errorMessage;
  final List<Location>? locationsList;
  final Location? location;
  final bool isLastPage;
}
