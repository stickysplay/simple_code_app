import 'dart:developer';

import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/character.dart';
import 'package:sc_app/repository/api.dart';

class CharactersRepo {
  CharactersRepo({required this.api});

  final Api api;

  Future<ResultCharactersRepo> filterByName(String name) async {
    try {
      final result = await api.dio.get(
        '/character/',
        queryParameters: {
          "name": name,
        },
      );
      final List charactersListJson = result.data['results'] ?? [];
      final charactersList = charactersListJson
          .map(
            (item) => Character.fromJson(item),
          )
          .toList();
      return ResultCharactersRepo(charactersList: charactersList);
    } catch (error) {
      log('CHARACTER_FETCH_ERROR: $error');
      return ResultCharactersRepo(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultCharactersRepo {
  ResultCharactersRepo({
    this.errorMessage,
    this.charactersList,
  });

  final String? errorMessage;
  final List<Character>? charactersList;
}
