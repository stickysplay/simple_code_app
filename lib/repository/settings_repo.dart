import 'package:shared_preferences/shared_preferences.dart';

class SettingsRepo {
  SharedPreferences? prefs;

  Future<void> init() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<bool?> saveLocal(String locale) async {
    return prefs?.setString('locale', locale);
  }

  Future<String?> readLocale() async {
    return prefs?.getString('locale');
  }

  Future<bool?> authorize() async {
    return prefs?.setBool('auth', true);
  }

  Future<bool?> isAuthorized() async {
    return prefs?.getBool('auth');
  }
}
