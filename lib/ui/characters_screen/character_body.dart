import 'package:flutter/material.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/character.dart';
import 'package:sc_app/ui/characters_screen/widgets/characters_grid_view.dart';
import 'package:sc_app/ui/characters_screen/widgets/characters_list_view.dart';
import 'package:sc_app/utils/constants.dart';

class CharactersBody extends StatefulWidget {
  const CharactersBody({
    Key? key,
    required this.charactersList,
  }) : super(key: key);

  final List<Character> charactersList;

  @override
  State<CharactersBody> createState() => _CharactersBodyState();
}

class _CharactersBodyState extends State<CharactersBody> {
  bool _isListView = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                S
                    .of(context)
                    .totalCharacters(widget.charactersList.length)
                    .toUpperCase(),
                style: AppStyles.s10w500.copyWith(color: AppColors.neutral3),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    _isListView = !_isListView;
                  });
                },
                child: Icon(
                  _isListView ? Icons.list : Icons.grid_view,
                  size: 24,
                  color: AppColors.neutral3,
                ),
              ),
            ],
          ),
        ),
        _isListView
            ? CharactersListView(
                characterList: widget.charactersList,
              )
            : CharactersGridView(
                characterList: widget.charactersList,
              ),
      ],
    );
  }
}
