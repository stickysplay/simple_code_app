import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/characters_bloc/characters_bloc.dart';
import 'package:sc_app/ui/characters_screen/character_body.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/widgets/custom_search_bar.dart';
import 'package:sc_app/utils/helpers/app_unfocuser.dart';
import 'package:sc_app/widgets/app_bottom_nav_bar.dart';
import 'package:sc_app/widgets/state_failure_widget.dart';

class CharactersScreen extends StatefulWidget {
  static const String routeName = '/characters';
  const CharactersScreen({super.key});

  @override
  State<CharactersScreen> createState() => _CharactersScreenState();
}

class _CharactersScreenState extends State<CharactersScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: AppUnfocuser(
        child: Scaffold(
          bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 0),
          appBar: AppBar(
            title: CustomSearchBar(
              hintText: S.of(context).searchCharacter,
              onChanged: (value) {
                context.read<CharactersBloc>().add(
                      CharactersEvent.filterByName(name: value),
                    );
              },
            ),
          ),
          body: BlocBuilder<CharactersBloc, CharactersState>(
            builder: (context, state) => state.map(
              loadInProgress: (_) => const Center(
                child: CircularProgressIndicator(),
              ),
              loadSuccess: (state) => CharactersBody(
                charactersList: state.charactersData,
              ),
              loadFailure: (state) => StateFailureWidget(
                text: S.of(context).notFoundCharacter,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
