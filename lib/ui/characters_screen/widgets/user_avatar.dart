import 'package:flutter/material.dart';
import 'package:sc_app/utils/app_assets.dart';

class UserAvatar extends StatelessWidget {
  const UserAvatar({
    super.key,
    required this.imageUrl,
    this.margin,
    this.border,
    this.radius = 36.0,
  });

  final BoxBorder? border;
  final EdgeInsets? margin;
  final double radius;
  final String? imageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: border,
      ),
      child: CircleAvatar(
        backgroundImage: imageUrl == null
            ? AssetImage(AppAssets.images.noAvatar) as ImageProvider
            : NetworkImage(imageUrl!),
        radius: radius,
      ),
    );
  }
}
