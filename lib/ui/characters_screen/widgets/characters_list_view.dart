import 'package:flutter/material.dart';
import 'package:sc_app/model/character.dart';
import 'package:sc_app/ui/characters_screen/widgets/character_list_tile.dart';

class CharactersListView extends StatelessWidget {
  const CharactersListView({
    required this.characterList,
    Key? key,
  }) : super(key: key);

  final List<Character> characterList;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: characterList.length,
        itemBuilder: (context, index) {
          final character = characterList[index];
          return CharacterListTile(character: character);
        },
      ),
    );
  }
}
