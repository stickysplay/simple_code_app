import 'package:flutter/material.dart';
import 'package:sc_app/ui/characters_screen/widgets/user_avatar.dart';
import 'package:sc_app/utils/constants.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/character.dart';

class CharacterGridTile extends StatelessWidget {
  const CharacterGridTile({
    Key? key,
    required this.character,
  }) : super(key: key);

  final Character character;

  String _statusLabel(String? status) {
    if (character.status == 'Alive') return S.current.alive.toUpperCase();
    if (character.status == 'Dead') return S.current.dead.toUpperCase();
    return S.current.noData;
  }

  Color _statusColor(String? status) {
    if (status == 'Alive') return AppColors.kLightGreen;
    if (status == 'Dead') return AppColors.kRed;
    return AppColors.neutral3;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        UserAvatar(
          imageUrl: character.image,
          radius: 60,
          margin: const EdgeInsets.only(bottom: 18),
        ),
        Text(
          _statusLabel(character.status),
          style: AppStyles.s10w500.copyWith(
            color: _statusColor(character.status),
          ),
        ),
        Text(
          character.name,
          style: AppStyles.s14w500.copyWith(height: 1.2),
        ),
        Text(
          '${character.species}, ${character.gender}',
          style: AppStyles.s12w400.copyWith(
            color: AppColors.neutral3,
            height: 1.2,
          ),
        ),
      ],
    );
  }
}
