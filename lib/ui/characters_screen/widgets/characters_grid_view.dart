import 'package:flutter/material.dart';
import 'package:sc_app/model/character.dart';
import 'package:sc_app/ui/characters_screen/widgets/character_grid_tile.dart';

class CharactersGridView extends StatelessWidget {
  const CharactersGridView({
    required this.characterList,
    Key? key,
  }) : super(key: key);

  final List<Character> characterList;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GridView.builder(
        padding: const EdgeInsets.symmetric(vertical: 16),
        itemCount: characterList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 20,
          childAspectRatio: 0.8,
        ),
        itemBuilder: (context, index) {
          final character = characterList[index];
          return CharacterGridTile(character: character);
        },
      ),
    );
  }
}
