import 'package:flutter/material.dart';
import 'package:sc_app/ui/characters_screen/widgets/user_avatar.dart';
import 'package:sc_app/utils/constants.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/character.dart';

class CharacterListTile extends StatelessWidget {
  const CharacterListTile({
    Key? key,
    required this.character,
  }) : super(key: key);

  final Character character;

  String _statusLabel(String? status) {
    if (character.status == 'Alive') return S.current.alive.toUpperCase();
    if (character.status == 'Dead') return S.current.dead.toUpperCase();
    return S.current.noData;
  }

  Color _statusColor(String? status) {
    if (status == 'Alive') return AppColors.kLightGreen;
    if (status == 'Dead') return AppColors.kRed;
    return AppColors.neutral3;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(
        children: [
          UserAvatar(
            imageUrl: character.image,
            margin: const EdgeInsets.only(right: 18),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                _statusLabel(character.status),
                style: AppStyles.s10w500.copyWith(
                  color: _statusColor(character.status),
                ),
              ),
              Text(
                character.name,
                style: AppStyles.s16w500.copyWith(height: 1.5),
              ),
              Text(
                '${character.species}, ${character.gender}',
                style: AppStyles.s12w400.copyWith(
                  color: AppColors.neutral3,
                  height: 1.5,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
