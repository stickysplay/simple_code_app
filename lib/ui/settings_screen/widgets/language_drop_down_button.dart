import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sc_app/generated/l10n.dart';

class LanguageDropDownButton extends StatefulWidget {
  const LanguageDropDownButton({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  final Function(String?) onChanged;

  @override
  State<LanguageDropDownButton> createState() => _LanguageDropDownButtonState();
}

class _LanguageDropDownButtonState extends State<LanguageDropDownButton> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('${S.of(context).language}: '),
        DropdownButton<String>(
          value: Intl.getCurrentLocale(),
          items: [
            DropdownMenuItem(
              value: 'en',
              child: Text(S.of(context).english),
            ),
            DropdownMenuItem(
              value: 'ru_RU',
              child: Text(S.of(context).russian),
            ),
          ],
          onChanged: widget.onChanged,
        ),
      ],
    );
  }
}
