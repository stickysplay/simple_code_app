import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/repository/settings_repo.dart';
import 'package:sc_app/ui/settings_screen/widgets/language_drop_down_button.dart';
import 'package:sc_app/utils/app_styles.dart';
import 'package:sc_app/widgets/app_bottom_nav_bar.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);
  static const String routeName = '/settings';

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 2),
      appBar: AppBar(
        title: Text(
          S.of(context).settings,
          style: AppStyles.s20w500,
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        elevation: 0,
      ),
      body: Center(
        child: LanguageDropDownButton(onChanged: _onChanged),
      ),
    );
  }

  void _onChanged(String? value) async {
    if (value == null) return;
    if (value == 'ru_RU') {
      await S.load(const Locale('ru', 'RU'));
      setState(() {});
    } else if (value == 'en') {
      await S.load(const Locale('en'));
      setState(() {});
    }
    if (!mounted) return;
    final settingsRepo = context.read<SettingsRepo>();
    settingsRepo.saveLocal(value);
  }
}
