import 'package:flutter/material.dart';
import 'package:sc_app/utils/constants.dart';

class EpisodeTile extends StatelessWidget {
  const EpisodeTile({
    super.key,
    required this.title,
    required this.subtitle,
    required this.subtitle2,
  });

  final String title, subtitle, subtitle2;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: BoxDecoration(
        gradient: const LinearGradient(
          colors: [
            Colors.blueGrey,
            Colors.cyan,
          ],
        ),
        borderRadius: BorderRadius.circular(8),
        boxShadow: const [
          BoxShadow(
            color: Colors.black45,
            offset: Offset(0, 2.5),
            blurRadius: 2,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: AppStyles.s14w500.copyWith(
                  color: Colors.white,
                ),
              ),
              Text(
                subtitle,
                style: AppStyles.s16w500.copyWith(
                  color: Colors.white,
                ),
              ),
              Text(
                subtitle2,
                style: AppStyles.s14w400.copyWith(
                  color: Colors.white,
                ),
              ),
            ],
          ),
          const Icon(
            Icons.keyboard_arrow_right,
            color: Colors.white,
          ),
        ],
      ),
    );
  }
}
