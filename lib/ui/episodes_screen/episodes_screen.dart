import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/episodes_bloc/episodes_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/ui/episodes_screen/episodes_body.dart';
import 'package:sc_app/widgets/app_alert_dialog.dart';
import 'package:sc_app/widgets/app_bottom_nav_bar.dart';

class EpisodesScreen extends StatelessWidget {
  const EpisodesScreen({Key? key}) : super(key: key);

  static const routeName = '/episodes';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).episodes),
        centerTitle: true,
      ),
      bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 2),
      body: Stack(
        children: [
          Positioned.fill(
            child: Column(
              children: [
                Expanded(
                  child: BlocBuilder<EpisodesBloc, EpisodesState>(
                    buildWhen: (previous, current) {
                      if (previous.runtimeType != current.runtimeType) {
                        return true;
                      } else {
                        final prevDataLength = previous.mapOrNull(
                          loadSuccess: (state) => state.episodesList.length,
                        );
                        final nextDataLength = current.mapOrNull(
                          loadSuccess: (state) => state.episodesList.length,
                        );
                        return prevDataLength != nextDataLength;
                      }
                    },
                    builder: (context, state) {
                      return state.map(
                        initial: (_) => const SizedBox.shrink(),
                        loadInProgress: (_) => const Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                        loadSuccess: (state) => EpisodesBody(
                          episodesList: state.episodesList,
                        ),
                        loadFailure: (state) => Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                S.of(context).somethingWentWrong,
                              ),
                              TextButton(
                                onPressed: () {
                                  context.read<EpisodesBloc>().add(
                                        const EpisodesEvent.fetchEpisodes(),
                                      );
                                },
                                child: Text(S.of(context).refresh),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          BlocConsumer<EpisodesBloc, EpisodesState>(
            builder: (context, state) {
              final isLoading = state.maybeMap(
                loadSuccess: (state) => state.isLoading,
                orElse: () => false,
              );
              return Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: isLoading
                    ? const LinearProgressIndicator()
                    : const SizedBox.shrink(),
              );
            },
            listener: (context, state) {
              state.mapOrNull(
                loadSuccess: (state) {
                  if (state.errorMessage != null) {
                    showDialog(
                      context: context,
                      builder: (context) => AppAlertDialog(
                        text: S.of(context).somethingWentWrong,
                      ),
                    );
                  }
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
