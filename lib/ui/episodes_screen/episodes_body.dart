import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/episodes_bloc/episodes_bloc.dart';
import 'package:sc_app/model/episode.dart';
import 'package:sc_app/ui/episodes_screen/widgets/episode_tile.dart';

class EpisodesBody extends StatelessWidget {
  const EpisodesBody({
    Key? key,
    required this.episodesList,
  }) : super(key: key);

  final List<Episode> episodesList;

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<EpisodesBloc>(context).add(
          const EpisodesEvent.fetchEpisodes(),
        );
      },
      child: NotificationListener<ScrollNotification>(
        onNotification: (notification) {
          if (notification.metrics.extentAfter == 0) {
            BlocProvider.of<EpisodesBloc>(context).add(
              const EpisodesEvent.nextPageEpisodes(),
            );
          }
          return false;
        },
        child: ListView.separated(
          padding: const EdgeInsets.only(
            top: 12,
            left: 12,
            right: 12,
          ),
          itemCount: episodesList.length,
          itemBuilder: (context, index) {
            final episode = episodesList[index];
            return EpisodeTile(
              title: episode.episode,
              subtitle: episode.name,
              subtitle2: episode.airDate,
            );
          },
          separatorBuilder: (context, _) => const SizedBox(height: 10),
        ),
      ),
    );
  }
}
