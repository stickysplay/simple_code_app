import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/location_details_bloc/location_details_bloc.dart';
import 'package:sc_app/model/location.dart';
import 'package:sc_app/ui/location_details_screen/location_details_screen.dart';
import 'package:sc_app/utils/constants.dart';

class LocationListTile extends StatelessWidget {
  const LocationListTile({
    required this.location,
    Key? key,
  }) : super(key: key);

  final Location location;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        location.type,
        style: AppStyles.s14w500.copyWith(color: AppColors.neutral3),
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            location.name,
            style: AppStyles.s14w500,
          ),
          Text(location.dimension),
        ],
      ),
      trailing: const Icon(
        Icons.keyboard_arrow_right,
        color: Colors.black,
      ),
      onTap: () {
        context.read<LocationDetailsBloc>().add(
              LocationDetailsEvent.fetchLocationDetails(id: location.id),
            );
        Navigator.of(context).pushNamed(LocationDetailsScreen.routeName);
      },
    );
  }
}
