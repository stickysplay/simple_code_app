import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/locations_bloc/locations_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/ui/locations_screen/locations_body.dart';
import 'package:sc_app/utils/helpers/app_unfocuser.dart';
import 'package:sc_app/widgets/app_alert_dialog.dart';
import 'package:sc_app/widgets/app_bottom_nav_bar.dart';
import 'package:sc_app/widgets/custom_search_bar.dart';
import 'package:sc_app/widgets/state_failure_widget.dart';

class LocationsScreen extends StatelessWidget {
  static const routeName = '/locations';
  const LocationsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String searchtext = '';
    return SafeArea(
      child: AppUnfocuser(
        child: Scaffold(
          bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 1),
          appBar: AppBar(
            title: CustomSearchBar(
              hintText: S.of(context).searchLocation,
              onChanged: (value) {
                searchtext = value;
                context.read<LocationsBloc>().add(
                      LocationsEvent.fetchLocations(locationName: value),
                    );
              },
            ),
          ),
          body: Stack(
            children: [
              Positioned.fill(
                child: BlocBuilder<LocationsBloc, LocationsState>(
                  buildWhen: (previous, current) {
                    if (previous.runtimeType != current.runtimeType) {
                      return true;
                    } else {
                      final prevDataLength = previous.mapOrNull(
                        loadSuccess: (state) => state.locationsData.length,
                      );
                      final nextDataLength = current.mapOrNull(
                        loadSuccess: (state) => state.locationsData.length,
                      );
                      return prevDataLength != nextDataLength;
                    }
                  },
                  builder: (context, state) => state.map(
                    initial: (_) => const SizedBox.shrink(),
                    loadInProgress: (_) => const Center(
                      child: CircularProgressIndicator.adaptive(),
                    ),
                    loadSuccess: (state) => LocationsBody(
                      locationsList: state.locationsData,
                      searchText: searchtext,
                    ),
                    loadFailure: (state) => StateFailureWidget(
                      text: S.of(context).notFoundLocation,
                    ),
                  ),
                ),
              ),
              BlocConsumer<LocationsBloc, LocationsState>(
                buildWhen: (previous, current) {
                  if (previous.runtimeType != current.runtimeType) {
                    return true;
                  } else {
                    final prevDataLength = previous.mapOrNull(
                      loadSuccess: (state) => state.locationsData.length,
                    );
                    final nextDataLength = current.mapOrNull(
                      loadSuccess: (state) => state.locationsData.length,
                    );
                    return prevDataLength != nextDataLength;
                  }
                },
                builder: (context, state) {
                  final isLoading = state.maybeMap(
                    loadSuccess: (state) => state.isLoading,
                    orElse: () => false,
                  );
                  return Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: isLoading
                        ? const LinearProgressIndicator()
                        : const SizedBox.shrink(),
                  );
                },
                listener: (context, state) {
                  state.mapOrNull(
                    loadSuccess: (state) {
                      if (state.errorMessage != null) {
                        showDialog(
                          context: context,
                          builder: (context) => AppAlertDialog(
                            text: S.of(context).somethingWentWrong,
                          ),
                        );
                      }
                      return;
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
