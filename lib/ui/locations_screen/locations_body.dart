import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/locations_bloc/locations_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/location.dart';
import 'package:sc_app/ui/locations_screen/widgets/locations_list_tile.dart';
import 'package:sc_app/utils/constants.dart';

class LocationsBody extends StatelessWidget {
  const LocationsBody({
    Key? key,
    required this.locationsList,
    required this.searchText,
  }) : super(key: key);

  final List<Location> locationsList;
  final String searchText;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                S
                    .of(context)
                    .totalLocations(locationsList.length)
                    .toUpperCase(),
                style: AppStyles.s10w500.copyWith(color: AppColors.neutral3),
              )
            ],
          ),
        ),
        Expanded(
          child: RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<LocationsBloc>(context).add(
                const LocationsEvent.fetchLocations(),
              );
            },
            child: NotificationListener<ScrollNotification>(
              onNotification: (notification) {
                if (notification.metrics.extentAfter == 0) {
                  BlocProvider.of<LocationsBloc>(context).add(
                    LocationsEvent.nextPageLocations(
                      name: searchText,
                    ),
                  );
                }
                return false;
              },
              child: ListView.separated(
                itemCount: locationsList.length,
                itemBuilder: (context, index) {
                  final location = locationsList[index];
                  return LocationListTile(location: location);
                },
                separatorBuilder: (_, __) => const Divider(thickness: 2),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
