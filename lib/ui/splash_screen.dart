import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/repository/settings_repo.dart';
import 'package:sc_app/ui/characters_screen/characters_screen.dart';
import 'package:sc_app/ui/login_screen/login_screen.dart';
import 'package:sc_app/utils/app_assets.dart';
import 'package:sc_app/utils/app_colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    final settingsRepo = context.read<SettingsRepo>();
    settingsRepo.init().whenComplete(
      () async {
        var defaultLocale = const Locale('ru', 'RU');
        final locale = await settingsRepo.readLocale();
        final isAuthorized = await settingsRepo.isAuthorized() ?? false;
        if (locale == 'en') {
          defaultLocale = const Locale('en');
        }
        S.load(defaultLocale).whenComplete(() {
          Navigator.of(context).pushNamedAndRemoveUntil(
            isAuthorized ? CharactersScreen.routeName : LoginScreen.routeName,
            (route) => false,
          );
        });
      },
    );

    super.initState();

    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: AppColors.mainText,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
      ),
    );
  }

  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: AppColors.background,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(
              AppAssets.images.splashBackground,
              fit: BoxFit.cover,
            ),
          ),
          Positioned.fill(
            child: Column(
              children: [
                Expanded(child: Image.asset(AppAssets.images.logo)),
                Expanded(child: Image.asset(AppAssets.images.splashMorty)),
                Expanded(
                    child: Image.asset(
                  AppAssets.images.splashRick,
                  fit: BoxFit.cover,
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
