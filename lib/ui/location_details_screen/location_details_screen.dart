import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sc_app/bloc/location_details_bloc/location_details_bloc.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/utils/app_styles.dart';
import 'package:sc_app/utils/helpers/date_formatter.dart';

class LocationDetailsScreen extends StatelessWidget {
  static const routeName = '/locationDetails';
  const LocationDetailsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<LocationDetailsBloc, LocationDetailsState>(
          builder: (context, state) => state.map(
            initial: (_) => const SizedBox.shrink(),
            loadInProgress: (_) =>
                const Center(child: CircularProgressIndicator()),
            loadSuccess: (state) {
              final location = state.location;
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      location.name,
                      style: AppStyles.s24w500,
                    ),
                    const SizedBox(height: 16),
                    Text(
                      '${location.type} · ${location.dimension}',
                      style: AppStyles.s20w400.copyWith(
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      '${S.of(context).premier}: ${location.created.dateStringify()}',
                      style: AppStyles.s20w400,
                    ),
                  ],
                ),
              );
            },
            loadFailure: (state) => Center(
              child: Text(state.errorText),
            ),
          ),
        ),
      ),
    );
  }
}
