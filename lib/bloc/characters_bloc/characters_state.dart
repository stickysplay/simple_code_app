part of 'characters_bloc.dart';

@freezed
class CharactersState with _$CharactersState {
  const factory CharactersState.loadInProgress() = _LoadInProgress;
  const factory CharactersState.loadSuccess(
      {required List<Character> charactersData}) = _LoadSuccess;
  const factory CharactersState.loadFailure(String errorText) = _LoadFailure;
}
