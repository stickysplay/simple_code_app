import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sc_app/generated/l10n.dart';
import 'package:sc_app/model/character.dart';
import 'package:sc_app/repository/characters_repo.dart';

part 'characters_event.dart';
part 'characters_state.dart';
part 'characters_bloc.freezed.dart';

class CharactersBloc extends Bloc<CharactersEvent, CharactersState> {
  final CharactersRepo repo;
  CharactersBloc({
    required this.repo,
  }) : super(const _LoadInProgress()) {
    on<_FilterByName>(_onFilterByName);
  }

  _onFilterByName(
    _FilterByName event,
    Emitter<CharactersState> emit,
  ) async {
    emit(const _LoadInProgress());
    final result = await repo.filterByName(event.name);
    if (result.errorMessage != null) {
      emit(
        _LoadFailure(S.current.somethingWentWrong),
      );
      return;
    }
    emit(
      _LoadSuccess(
        charactersData: result.charactersList!,
      ),
    );
  }
}
