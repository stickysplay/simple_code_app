import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sc_app/model/location.dart';
import 'package:sc_app/repository/locations_repo.dart';

part 'locations_event.dart';
part 'locations_state.dart';
part 'locations_bloc.freezed.dart';

class LocationsBloc extends Bloc<LocationsEvent, LocationsState> {
  LocationsBloc({
    required this.repo,
  }) : super(const _Initial()) {
    on<_FetchLocations>(_onFetchLocations);
    on<_NextPageLocations>(_nextPage);
  }

  final LocationsRepo repo;

  int _currentPage = 1;
  bool _isLastPage = false;
  bool _isInProgress = false;

  Future<void> _onFetchLocations(
    _FetchLocations event,
    Emitter<LocationsState> emit,
  ) async {
    emit(const _LoadInProgress());

    _currentPage = 1;
    _isLastPage = false;
    _isInProgress = false;

    final result = await repo.fetchLocation(
      event.locationName ?? '',
    );

    if (result.errorMessage != null) {
      emit(
        _LoadFailure(result.errorMessage.toString()),
      );
      return;
    }
    emit(
      _LoadSuccess(
        locationsData: result.locationsList!,
      ),
    );
  }

  Future<void> _nextPage(
    _NextPageLocations event,
    Emitter<LocationsState> emit,
  ) async {
    if (_isLastPage) return;
    if (_isInProgress) return;
    _isInProgress = true;

    final currentData = state.maybeMap(
      loadSuccess: (state) => state.locationsData,
      orElse: () => <Location>[],
    );

    emit(
      _LoadSuccess(
        locationsData: currentData,
        isLoading: true,
      ),
    );

    final result = await repo.nextPage(_currentPage + 1, event.name);
    if (result.errorMessage != null) {
      emit(
        _LoadSuccess(
          locationsData: currentData,
        ),
      );
      return;
    }
    emit(
      _LoadSuccess(
        locationsData: [
          ...currentData,
          ...result.locationsList!,
        ],
      ),
    );
    _currentPage++;
    _isLastPage = result.isLastPage;
    _isInProgress = false;
  }
}
