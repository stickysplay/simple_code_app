part of 'locations_bloc.dart';

@freezed
class LocationsState with _$LocationsState {
  const factory LocationsState.initial() = _Initial;
  const factory LocationsState.loadInProgress() = _LoadInProgress;
  const factory LocationsState.loadSuccess({
    required List<Location> locationsData,
    @Default(false) bool isLoading,
    final String? errorMessage,
  }) = _LoadSuccess;
  const factory LocationsState.loadFailure(String errorText) = _LoadFailure;
}
