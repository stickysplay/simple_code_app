part of 'location_details_bloc.dart';

@freezed
class LocationDetailsState with _$LocationDetailsState {
  const factory LocationDetailsState.initial() = _Initial;
  const factory LocationDetailsState.loadInProgress() = _LoadInProgress;
  const factory LocationDetailsState.loadSuccess({
    required Location location,
  }) = _LoadSuccess;
  const factory LocationDetailsState.loadFailure(String errorText) = _LoadFailure;
}
