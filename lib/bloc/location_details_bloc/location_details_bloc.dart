import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sc_app/model/location.dart';
import 'package:sc_app/repository/locations_repo.dart';

part 'location_details_event.dart';
part 'location_details_state.dart';
part 'location_details_bloc.freezed.dart';

class LocationDetailsBloc
    extends Bloc<LocationDetailsEvent, LocationDetailsState> {
  final LocationsRepo repo;

  LocationDetailsBloc({
    required this.repo,
  }) : super(const _Initial()) {
    on<_FetchLocationDetails>(_onFetchLocationDetails);
  }

  _onFetchLocationDetails(
    _FetchLocationDetails event,
    Emitter<LocationDetailsState> emit,
  ) async {
    emit(const _LoadInProgress());

    final result = await repo.fetchLocationDetails(event.id);

    if (result.errorMessage != null) {
      emit(
        _LoadFailure(result.errorMessage.toString()),
      );
      return;
    }
    emit(_LoadSuccess(location: result.location!));
  }
}
