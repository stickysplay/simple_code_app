part of 'location_details_bloc.dart';

@freezed
class LocationDetailsEvent with _$LocationDetailsEvent {
  const factory LocationDetailsEvent.started() = _Started;
  const factory LocationDetailsEvent.fetchLocationDetails({required int id}) =
      _FetchLocationDetails;
}
