part of 'episodes_bloc.dart';

@freezed
class EpisodesEvent with _$EpisodesEvent {
  const factory EpisodesEvent.started() = _Started;
  const factory EpisodesEvent.fetchEpisodes() = _FetchEpisodes;
  const factory EpisodesEvent.nextPageEpisodes() = _NextPageEpisodes;
}
