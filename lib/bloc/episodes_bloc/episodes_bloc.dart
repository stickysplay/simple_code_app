import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:sc_app/model/episode.dart';
import 'package:sc_app/repository/episodes_repo.dart';

part 'episodes_event.dart';
part 'episodes_state.dart';
part 'episodes_bloc.freezed.dart';

class EpisodesBloc extends Bloc<EpisodesEvent, EpisodesState> {
  EpisodesBloc({
    required this.repo,
  }) : super(const _Initial()) {
    on<_FetchEpisodes>(_onFetchEpisodes);
    on<_NextPageEpisodes>(_nextPage);
  }
  final EpisodesRepo repo;

  int _currentPage = 1;
  bool _isLastPage = false;
  bool _isInProgress = false;

  Future<void> _onFetchEpisodes(
    _FetchEpisodes event,
    Emitter<EpisodesState> emit,
  ) async {
    emit(const _LoadInProgress());
    _currentPage = 1;
    _isLastPage = false;
    final result = await repo.fetchEpisodes();
    if (result.errorMessage != null) {
      emit(
        _LoadFailure(result.errorMessage!),
      );
      return;
    }
    emit(
      _LoadSuccess(episodesList: result.episodesList!),
    );
  }

  Future<void> _nextPage(
    _NextPageEpisodes event,
    Emitter<EpisodesState> emit,
  ) async {
    if (_isLastPage) return;
    if (_isInProgress) return;
    _isInProgress = true;

    final currentData = state.maybeMap(
      loadSuccess: (state) => state.episodesList,
      orElse: () => <Episode>[],
    );

    emit(
      _LoadSuccess(
        episodesList: currentData,
        isLoading: true,
      ),
    );

    final result = await repo.nextPage(_currentPage + 1);
    if (result.errorMessage != null) {
      emit(
        _LoadSuccess(
          episodesList: currentData,
          errorMessage: result.errorMessage,
        ),
      );
      return;
    }
    emit(
      _LoadSuccess(
        episodesList: [
          ...currentData,
          ...result.episodesList!,
        ],
      ),
    );
    _currentPage++;
    _isLastPage = result.isLastpage;
    _isInProgress = false;
  }
}
