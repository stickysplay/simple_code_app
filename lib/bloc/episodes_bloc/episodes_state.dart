part of 'episodes_bloc.dart';

@freezed
class EpisodesState with _$EpisodesState {
  const factory EpisodesState.initial() = _Initial;
  const factory EpisodesState.loadInProgress() = _LoadInProgress;
  const factory EpisodesState.loadFailure(String errorText) = _LoadFailure;
  const factory EpisodesState.loadSuccess({
    required List<Episode> episodesList,
    @Default(false) bool isLoading,
    String? errorMessage,
  }) = _LoadSuccess;
}
