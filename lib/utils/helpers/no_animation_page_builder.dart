import 'package:flutter/material.dart';

class NoAnimationPageRouteBuilder extends PageRouteBuilder {
  NoAnimationPageRouteBuilder({
    required this.route,
  }) : super(
          pageBuilder: (context, animatiom, secondaryAnimation) => route,
        );

  final Widget route;
}
