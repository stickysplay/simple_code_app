import 'package:intl/intl.dart';

extension DateParser on DateTime {
  String dateStringify() {
    return DateFormat(
      'EEEE dd MMMM yyyy',
    ).format(this);
  }
}
