abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

  final String noAvatar = 'assets/images/bitmap/no_avatar.png';
  final String logo = 'assets/images/bitmap/logo.png';
  final String splashBackground = 'assets/images/bitmap/splash_background.png';
  final String splashRick = 'assets/images/bitmap/splash_rick.png';
  final String splashMorty = 'assets/images/bitmap/splash_morty.png';
  final String notFoundCharacter =
      'assets/images/bitmap/not_found_character.png';
}

class _Svg {
  const _Svg();

  final String account = 'assets/images/svg/account.svg';
  final String password = 'assets/images/svg/password.svg';
  final String obscure = 'assets/images/svg/obscure.svg';
  final String characters = 'assets/images/svg/characters.svg';
  final String settings = 'assets/images/svg/settings.svg';
  final String locations = 'assets/images/svg/locations.svg';
  final String episode = 'assets/images/svg/episode.svg';
}
