import 'package:flutter/cupertino.dart';
import 'package:sc_app/utils/app_colors.dart';

class AppStyles {
  static const s10w500 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s12w400 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.mainText,
  );

  static const s14w400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s14w500 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
    height: 1.4,
    color: AppColors.mainText,
  );

  static const s16w400 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s16w500 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s18w500 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s20w400 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s20w500 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w500,
    color: AppColors.mainText,
    height: 1.4,
  );

  static const s24w500 = TextStyle(
    fontSize: 24,
    fontWeight: FontWeight.w500,
    color: AppColors.mainText,
    height: 1.4,
  );
}
