import 'package:flutter/material.dart';
import 'package:sc_app/utils/constants.dart';

ThemeData appTheme = ThemeData(
  //Здесь можно useMaterial3: true поставить. но тогда цвета приложения начнут меняться динамически
  // ignore: deprecated_member_use
  androidOverscrollIndicator: AndroidOverscrollIndicator.stretch,
  primaryColor: AppColors.primary,
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      textStyle: AppStyles.s16w400,
      primary: AppColors.primary,
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      primary: AppColors.primary,
      side: const BorderSide(color: AppColors.primary),
      textStyle: AppStyles.s16w400,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
    ),
  ),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    selectedLabelStyle: AppStyles.s12w400.copyWith(color: AppColors.primary),
    selectedItemColor: AppColors.primary,
    unselectedLabelStyle: AppStyles.s12w400.copyWith(color: AppColors.neutral2),
    unselectedItemColor: AppColors.neutral2,
  ),
  appBarTheme: const AppBarTheme(
    backgroundColor: AppColors.background,
    foregroundColor: AppColors.mainText,
    elevation: 0,
  ),
  scaffoldBackgroundColor: AppColors.background,
);
